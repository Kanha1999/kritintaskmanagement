﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace JwtAuthentication.ViewModels
{
    
    public class Reply
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public string res { get; set; }
        public object Response { get; set; }
    }


}
