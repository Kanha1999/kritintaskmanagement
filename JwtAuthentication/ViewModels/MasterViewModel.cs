﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace JwtAuthentication.ViewModels
{
    public class MasterViewModel
    {
        public class Reply
        {
            public int Status { get; set; }
            public string Message { get; set; }
            // public string res { get; set; }
            public object Response { get; set; }
        }
        

        //Project Random quote
        public class GetQuote
        {
            public int Q_id { get; set; }
            public string Quote { get; set; }
        }
        public class ShowQuote
        {
            public string Quote { get; set; }
        }
        //End Project Random quote

        public class Emp
        {
            public int EmpId { get; set; }
            public string EmpName { get; set; }
        }
        public class  TaskD
        {
            public int TaskId { get; set; }
            public string Task { get; set; }
            public int Responsible { get; set; }
            public string ResponsibleName { get; set; }
            public DateTime CreatedAt { get; set; }
            public string Deadline { get; set; }
            public string Category { get; set; }
            public int PriorityId { get; set; }
            public string Priority { get; set; }
            public string Status { get; set; }
            public int StatusId { get; set; }
            public string SubmittedOn { get; set; }
            public string Notes { get; set; }
            public List<int> SupportedBy { get; set; } = new List<int>();
            public List<string> SupportedByName { get; set; } = new List<string>();
            public List<string> Action{ get; set; } = new List<string>();

        }
        
        

    }//end class invert

    
}
