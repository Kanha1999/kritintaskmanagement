﻿using System;
using System.Collections.Generic;
using System.Data;
using JwtAuthentication.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Npgsql;
using JwtAuthentication.EntityModel;
using static JwtAuthentication.ViewModels.MasterViewModel;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.Extensions.Configuration;
//using Reply = JwtAuthentication.ViewModels.MasterViewModel.Reply;

namespace JwtAuthentication.Controllers
{
    //Author : Mahendra
    //Date : 06/08/2019
    //[EnableCors("AllowFromAll")]
    //[Authorize]
    //[Route("V1/[controller]")]
    //[ApiController]

    public class MasterController : ControllerBase
    {
        MastersEntityModel master = new MastersEntityModel();








        //Project Random quote
        [HttpPost]
        [Route("~/api/AddQuote")]// for login by user
        public MasterViewModel.Reply AddQuote([FromBody]GetQuote model)
        {
            MasterViewModel.Reply res = new MasterViewModel.Reply();
            try
            {
                res = master.AddQuote(model);
                return res;
            }
            catch (Exception ex) // AddQuote
            {
                res.Status = 500;
                res.Message = ex.Message;
                res.Response = null;
                return res;
            }
        } // End AddQuote
        [HttpGet]
        [Route("~/api/ShowQuote")]
        public MasterViewModel.Reply ShowQuote()
        {
            MasterViewModel.Reply res = new MasterViewModel.Reply();
            try
            {
                res = master.ShowQuote();
                return res;
            }
            catch (Exception ex)//ShowQuote
            {
                res.Status = 500;
                res.Message = ex.Message;
                res.Response = null;
                return res;
            }
        }//End ShowQuote
         //End Project Random quote










        //adding new employee
        [HttpPost]
        [Route("~/api/AddEmp")]// for login by user
        public MasterViewModel.Reply AddEmp([FromBody]Emp model)
        {
            MasterViewModel.Reply res = new MasterViewModel.Reply();
            try
            {
                res = master.AddEmp(model);
                return res;
            }
            catch (Exception ex) // AddQuote
            {
                res.Status = 500;
                res.Message = ex.Message;
                res.Response = null;
                return res;
            }
        } // End AddEmp
        //show all employees
        [HttpGet]
        [Route("~/api/ShowAllEmp")]
        public MasterViewModel.Reply ShowAllEmp()
        {
            MasterViewModel.Reply res = new MasterViewModel.Reply();
            try
            {
                res = master.ShowAllEmp();
                return res;
            }
            catch (Exception ex)//ShowEmp
            {
                res.Status = 500;
                res.Message = ex.Message;
                res.Response = null;
                return res;
            }
        }//End ShowEmp

        //to do show emp by empid

        [HttpPost]
        [Route("~/api/AddTaskAdmin")]// for Add task and assign to emp
        public MasterViewModel.Reply AddTaskAdmin([FromBody]TaskD model)
        {
            MasterViewModel.Reply res = new MasterViewModel.Reply();
            try
            {
                res = master.AddTaskAdmin(model);
                return res;
            }
            catch (Exception ex) // Add task and assign to emp
            {
                res.Status = 500;
                res.Message = ex.Message;
                res.Response = null;
                return res;
            }
        } // End Add task and assign to emp      
        [HttpPost]
        [Route("~/api/AddTaskEmp")]// change task status and add notes also time compaired with deadline to add late in status in task , status 0:in progress  1:completed
        public MasterViewModel.Reply AddTaskEmp([FromBody]TaskD model)
        {
            MasterViewModel.Reply res = new MasterViewModel.Reply();
            try
            {
                res = master.AddTaskEmp(model);
                return res;
            }
            catch (Exception ex) 
            {
                res.Status = 500;
                res.Message = ex.Message;
                res.Response = null;
                return res;
            }
        } // End AddTaskEmp      



        [HttpGet]
        [Route("~/api/ShowAllTask")]//for showing all task
        public MasterViewModel.Reply ShowAllTask()
        {
            MasterViewModel.Reply res = new MasterViewModel.Reply();
            try
            {
                res = master.ShowAllTask();
                return res;
            }
            catch (Exception ex)//ShowEmp
            {
                res.Status = 500;
                res.Message = ex.Message;
                res.Response = null;
                return res;
            }
        }//End ShowAllTask
        [HttpGet]
        [Route("~/api/ShowByTaskId")]//show task for given task_id
        public MasterViewModel.Reply ShowByTaskId(int taskId)
        {
            MasterViewModel.Reply res = new MasterViewModel.Reply();
            try
            {
                res = master.ShowByTaskId(taskId);
                return res;
            }
            catch (Exception ex)//ShowTaskByEmpId
            {
                res.Status = 500;
                res.Message = ex.Message;
                res.Response = null;
                return res;
            }
        }//End ShowByTaskId
        [HttpGet]
        [Route("~/api/ShowByEmpId")]//show task for given emp_id
        public MasterViewModel.Reply ShowByEmpId(int empId)
        {
            MasterViewModel.Reply res = new MasterViewModel.Reply();
            try
            {
                res = master.ShowByEmpId(empId);
                return res;
            }
            catch (Exception ex)//ShowTask
            {
                res.Status = 500;
                res.Message = ex.Message;
                res.Response = null;
                return res;
            }
        }//End ShowByEmpId
        [HttpGet]
        [Route("~/api/ShowByEmp")] //show task for given emp_name
        public MasterViewModel.Reply ShowByEmp(string empName)
        {
            MasterViewModel.Reply res = new MasterViewModel.Reply();
            try
            {
                res = master.ShowByEmp(empName);
                return res;
            }
            catch (Exception ex)//ShowTask
            {
                res.Status = 500;
                res.Message = ex.Message;
                res.Response = null;
                return res;
            }
        }//End ShowByEmp
        [HttpGet]
        [Route("~/api/ShowByStatusId")]// show task a/c to given status
        public MasterViewModel.Reply ShowByStatusId(int statusId)
        {
            MasterViewModel.Reply res = new MasterViewModel.Reply();
            try
            {
                res = master.ShowByStatusId(statusId);
                return res;
            }
            catch (Exception ex)//ShowTask
            {
                res.Status = 500;
                res.Message = ex.Message;
                res.Response = null;
                return res;
            }
        }//End ShowByStatusId
        [HttpGet]
        [Route("~/api/ShowByPriorityId")]// show task a/c to given priority
        public MasterViewModel.Reply ShowByPriorityId(int PriorityId)
        {
            MasterViewModel.Reply res = new MasterViewModel.Reply();
            try
            {
                res = master.ShowByPriorityId(PriorityId);
                return res;
            }
            catch (Exception ex)//ShowTask
            {
                res.Status = 500;
                res.Message = ex.Message;
                res.Response = null;
                return res;
            }
        }//End ShowByPriorityId
        [HttpGet]
        [Route("~/api/ShowBycategoryId")]//show task a/c to given catetgory Id
        public MasterViewModel.Reply ShowBycategoryId(int categoryId)
        {
            MasterViewModel.Reply res = new MasterViewModel.Reply();
            try
            {
                res = master.ShowBycategoryId(categoryId);
                return res;
            }
            catch (Exception ex)//ShowTask
            {
                res.Status = 500;
                res.Message = ex.Message;
                res.Response = null;
                return res;
            }
        }//End ShowBycategoryId
        [HttpGet]
        [Route("~/api/ShowByCategoryName")]//show task a/c to given category name
        public MasterViewModel.Reply ShowByCategoryName(string categoryName)
        {
            MasterViewModel.Reply res = new MasterViewModel.Reply();
            try
            {
                res = master.ShowByCategoryName(categoryName);
                return res;
            }
            catch (Exception ex)//ShowTask
            {
                res.Status = 500;
                res.Message = ex.Message;
                res.Response = null;
                return res;
            }
        }//End ShowByCategoryName
        [HttpGet]
        [Route("~/api/ShowBysupportedById")]//show task a/c to given supporter emp
        public MasterViewModel.Reply ShowBysupportedById(int empId)
        {
            MasterViewModel.Reply res = new MasterViewModel.Reply();
            try
            {
                res = master.ShowBysupportedById(empId);
                return res;
            }
            catch (Exception ex)//ShowTask
            {
                res.Status = 500;
                res.Message = ex.Message;
                res.Response = null;
                return res;
            }
        }//End ShowBysupportedById


    }
}
