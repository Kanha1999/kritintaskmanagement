﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Npgsql;
using System.Net.Mail;
using System.Net;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

public class DataConnection
{
    
    //Todo: Remove Unuseful functions
    public string constr { get; set; }
    public SqlConnection con { get; set; }
    public NpgsqlConnection npgcon { get; set; }
    SqlCommand cmd = new SqlCommand();
    DataTable DtTable;
    SqlDataAdapter Da;
    DataSet Ds;
    string strqry;
    NpgsqlDataAdapter NpgDa;
    public List<string> paramlist { get; set; }
    public List<string> objlist { get; set; }
    public DataConnection()
    {
        paramlist = new List<string>();
        objlist = new List<string>();
    }
    public NpgsqlConnection pgconnect()
    {
        try
        {
            constr = JwtAuthentication.Startup.ConnectionString;
            NpgsqlConnection npgcon = new NpgsqlConnection();
            npgcon.ConnectionString = constr;
            if (npgcon.State == ConnectionState.Open)
                npgcon.Close();
            npgcon.Open();
            return npgcon;
        }
        catch (Exception ex)
        {
            return null;
        }
    }
    public static string smtpConfig(string toAddress, string subject, string body, string ccTo)
    {
        try
        {
            MailMessage mObj = new MailMessage();
            mObj.To.Add(toAddress);
            if (ccTo != "")
                mObj.CC.Add(ccTo);
            mObj.From = new MailAddress("support@leaguegames.in");
            mObj.Subject = subject;
            mObj.Body = body;
            mObj.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtpout.asia.secureserver.net";//
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = new
            NetworkCredential("support@leaguegames.in", "leaguegames@2018");
            smtp.Send(mObj);
            return "Send Message on your mail id !";
        }
        catch (Exception ex)
        {
            return ex.ToString();
            throw ex;
        }
    }

    public SqlConnection Connect()
    {
        try
        {
            constr = System.Configuration.ConfigurationManager.ConnectionStrings["fantasysports"].ToString();
            con = new SqlConnection();
            con.ConnectionString = constr;
            if (con.State == ConnectionState.Open)
            {
                con.Close();
                con = new SqlConnection();
                con.ConnectionString = constr;
            }
            con.Open();
            return con;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void Close()
    {
        try
        {
            if (con != null)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }

    public DataTable GetDatatable(string strsql)
    {
        try
        {
            con = new SqlConnection();
            con = Connect();
            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strsql;
            cmd.ExecuteNonQuery();
            Da = new SqlDataAdapter();
            Ds = new DataSet();
            DtTable = new DataTable();
            Da.SelectCommand = cmd;
            Da.Fill(Ds);
            DtTable = Ds.Tables[0];
            return DtTable;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            Close();
        }
    }


    public DataTable GetNpgDatatableStoreProc(string Storeprocedure)
    {
        try
        {
            NpgsqlTransaction tr = null;
            npgcon = pgconnect();

            tr = npgcon.BeginTransaction();
            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.Connection = pgconnect();
            cmd.CommandText = Storeprocedure;
            cmd.CommandType = CommandType.StoredProcedure;
            NpgDa = new NpgsqlDataAdapter(cmd);
            Ds = new DataSet();
            DtTable = new DataTable();
            NpgDa.Fill(Ds);
            DtTable = Ds.Tables[0];
            tr.Commit();
            npgcon.Close();
            return DtTable;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            Close();
        }
    }
    public DataTable GetDatatableStoreProc(string Storeprocedure)
    {
        try
        {
            con = new SqlConnection();
            con = Connect();
            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = Storeprocedure;
            cmd.ExecuteNonQuery();
            Da = new SqlDataAdapter();
            Ds = new DataSet();
            DtTable = new DataTable();
            Da.SelectCommand = cmd;
            Da.Fill(Ds);
            DtTable = Ds.Tables[0];
            return DtTable;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            Close();
        }
    }
    public DataTable GetDatatableStoreprocedure(string Storeprocedure, List<string> Pname, List<object> PValue, int cnt)
    {
        try
        {
            con = new SqlConnection();
            con = Connect();
            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = Storeprocedure;
            for (int i = 0; i <= cnt; i++)
            {
                cmd.Parameters.AddWithValue(Pname[i], PValue[i]);
            }
            cmd.ExecuteNonQuery();
            Da = new SqlDataAdapter();
            Ds = new DataSet();
            DtTable = new DataTable();
            Da.SelectCommand = cmd;
            Da.Fill(Ds);
            DtTable = Ds.Tables[0];
        }
        catch (Exception ex)
        {
            //    Utilily.UserMessage(ex.Message);
            //   Utilily.LogException(ex, "Error Occured While Calling " + Storeprocedure);
            //  Utilily.ManageUserLog("0", ex.Message, "Utility.svc/");
        }
        finally
        {
            Close();
        }
        return DtTable;
    }

    //public void ExecuteNonQuery(string storeprocedurename, List<string> pname, List<object> pvalue)
    //{
    //    SqlTransaction tr = null;
    //    try
    //    {
    //        con = new SqlConnection();
    //        con = Connect();
    //        cmd = new SqlCommand();
    //        cmd.Connection = con;

    //        tr = con.BeginTransaction();
    //        cmd.CommandType = CommandType.StoredProcedure;
    //        cmd.CommandText = storeprocedurename;
    //        cmd.Transaction = tr;

    //        for (int i = 0; i < pname.Count(); i++)
    //        {
    //            cmd.Parameters.AddWithValue("@" + pname[i], pvalue[i]);
    //        }
    //        cmd.ExecuteNonQuery();
    //        tr.Commit();
    //        con.Close();
    //    }
    //    catch (Exception ex)
    //    {
    //        if (tr != null)
    //        {
    //            tr.Rollback();
    //        }
    //      //  Utilily.UserMessage(ex.Message);
    //        throw (ex);
    //    }
    //    finally
    //    {
    //        Close();
    //    }
    //}
    //public int ExecutePgsqlScaler(string functionName, List<string> fnparam, List<object> fnvalues)
    //{
    //    NpgsqlTransaction tr = null;
    //    npgcon = pgconnect();

    //    tr = npgcon.BeginTransaction();
    //    int lastInsertId = 0;
    //    NpgsqlCommand cmd = new NpgsqlCommand();
    //    cmd.Connection = npgcon;
    //    cmd.CommandText = functionName;
    //    cmd.CommandType = CommandType.StoredProcedure;
    //    for (int i = 0; i < fnparam.Count - 1; i++)
    //    {
    //        cmd.Parameters.AddWithValue(fnparam[i], fnvalues[i]);
    //    }
    //    lastInsertId = Convert.ToInt32(cmd.ExecuteScalar());
    //    tr.Commit();
    //    npgcon.Close();
    //    return lastInsertId;

    //}
    //public int ExecutePgScaler(string functionName, List<string> fnparam, List<string> fnvalues)
    //{
    //    NpgsqlTransaction tr = null;
    //    npgcon = pgconnect();

    //    tr = npgcon.BeginTransaction();
    //    int lastInsertId = 0;
    //    NpgsqlCommand cmd = new NpgsqlCommand();
    //    cmd.Connection = npgcon;
    //    cmd.CommandText = functionName;
    //    cmd.CommandType = CommandType.StoredProcedure;
    //    for (int i = 0; i < fnparam.Count - 1; i++)
    //    {
    //        cmd.Parameters.AddWithValue(fnparam[i], fnvalues[i]);
    //    }
    //    lastInsertId = Convert.ToInt32(cmd.ExecuteScalar());
    //    tr.Commit();
    //    npgcon.Close();
    //    return lastInsertId;

    //}
    //public int ExecuteScaler(string storeprocedurename, List<string> pname, List<object> pvalue)
    //{
    //    SqlTransaction tr = null;
    //    int MaxId = 0;
    //    try
    //    {
    //        con = new SqlConnection();
    //        con = Connect();
    //        cmd = new SqlCommand();
    //        cmd.Connection = con;

    //        tr = con.BeginTransaction();
    //        cmd.CommandType = CommandType.StoredProcedure;
    //        cmd.CommandText = storeprocedurename;
    //        cmd.Transaction = tr;

    //        for (int i = 0; i < pname.Count(); i++)
    //        {
    //            cmd.Parameters.AddWithValue("@" + pname[i], pvalue[i]);
    //        }
    //        MaxId = Convert.ToInt32(cmd.ExecuteScalar());
    //        tr.Commit();
    //        con.Close();
    //    }
    //    catch (Exception ex)
    //    {
    //        if (tr != null)
    //        {
    //            tr.Rollback();
    //        }
    //       // Utilily.UserMessage(ex.Message);
    //        throw (ex);
    //    }
    //    finally
    //    {
    //        Close();

    //    }
    //    return MaxId;
    //}
    //public string deleteSalesChangeMapping(Statevar Sv)
    //{


    //    try
    //    {
    //        SqlCommand cmd = new SqlCommand();

    //        con = new SqlConnection();
    //        con = Connect();
    //        cmd = new SqlCommand();
    //        cmd.Connection = con;



    //        cmd.CommandType = CommandType.StoredProcedure;
    //        cmd.CommandText = "Sp_CkeckPurchaseMapping";
    //        cmd.Connection = con;
    //        cmd.Parameters.AddWithValue("@BranchID", Sv.BranchID);
    //        cmd.Parameters.AddWithValue("@CompanyID", Sv.CompanyID);
    //        cmd.Parameters.AddWithValue("@LedgerID", Sv.LedgerID);
    //        cmd.Parameters.AddWithValue("@Flag", "delSalesMapping");
    //        cmd.ExecuteNonQuery();
    //        cmd.Parameters.Clear();

    //        strqry = " Data Deleted Successfully!";

    //    }
    //    catch (Exception ex)
    //    {

    //        strqry = "Sorry ! There is an error" + ex;
    //    }
    //    return strqry;
    //}
    //public DataSet GetDataSetStoreprocedure(string Storeprocedure, List<string> Pname, List<object> PValue, int cnt)
    //{
    //    try
    //    {
    //        con = new SqlConnection();
    //        con = Connect();
    //        cmd = new SqlCommand();
    //        cmd.Connection = con;
    //        cmd.CommandType = CommandType.StoredProcedure;
    //        cmd.CommandText = Storeprocedure;
    //        for (int i = 0; i <= cnt; i++)
    //        {
    //            cmd.Parameters.AddWithValue(Pname[i], PValue[i]);
    //        }
    //        cmd.ExecuteNonQuery();
    //        Da = new SqlDataAdapter();
    //        Ds = new DataSet();
    //        DtTable = new DataTable();
    //        Da.SelectCommand = cmd;
    //        Da.Fill(Ds);

    //    }
    //    catch (Exception ex)
    //    {
    //       // Utilily.UserMessage(ex.Message);
    //    }
    //    finally
    //    {
    //        Close();
    //    }
    //    return Ds;
    //}
    //public DataSet GetDataSetStoreprocedure(string Storeprocedure)
    //{
    //    try
    //    {
    //        con = new SqlConnection();
    //        con = Connect();
    //        cmd = new SqlCommand();
    //        cmd.Connection = con;
    //        cmd.CommandType = CommandType.StoredProcedure;
    //        cmd.CommandText = Storeprocedure;
    //        cmd.ExecuteNonQuery();
    //        Da = new SqlDataAdapter();
    //        Ds = new DataSet();
    //        DtTable = new DataTable();
    //        Da.SelectCommand = cmd;
    //        Da.Fill(Ds);

    //    }
    //    catch (Exception ex)
    //    {
    //       // Utilily.UserMessage(ex.Message);
    //    }
    //    finally
    //    {
    //        Close();
    //    }
    //    return Ds;
    //}
    //public void GetGrid(GridView GrdView, string strqry)
    //{
    //    try
    //    {
    //        con = Connect();
    //        cmd = new SqlCommand();
    //        cmd.Connection = con;
    //        cmd.CommandType = CommandType.Text;
    //        cmd.CommandText = strqry;
    //        cmd.ExecuteNonQuery();
    //        Da = new SqlDataAdapter();
    //        Ds = new DataSet();
    //        DtTable = new DataTable();
    //        Da.SelectCommand = cmd;
    //        Da.Fill(Ds);
    //        DtTable = Ds.Tables[0];
    //        if (DtTable.Rows.Count > 0)
    //        {
    //            GrdView.DataSource = DtTable;
    //            GrdView.DataBind();

    //        }
    //        else
    //        {
    //            GrdView.DataSource = null;
    //            GrdView.DataBind();

    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //    finally
    //    {
    //        Close();
    //    }

    //}
    //public void FillDropDownList(DropDownList Ddl, string strqry, string datatext, string datavalue)
    //{
    //    try
    //    {
    //        con = Connect();
    //        cmd = new SqlCommand();
    //        cmd.Connection = con;
    //        cmd.CommandType = CommandType.Text;
    //        cmd.CommandText = strqry;
    //        cmd.ExecuteNonQuery();
    //        Da = new SqlDataAdapter();
    //        Ds = new DataSet();
    //        DtTable = new DataTable();
    //        Da.SelectCommand = cmd;
    //        Da.Fill(Ds);
    //        DtTable = Ds.Tables[0];
    //        if (DtTable.Rows.Count > 0)
    //        {
    //            Ddl.DataSource = DtTable;
    //            Ddl.DataTextField = datatext;
    //            Ddl.DataValueField = datavalue;
    //            Ddl.DataBind();

    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //    finally
    //    {
    //        Close();
    //    }
    //}
    //public void FillDropDownListWithDataTable(DropDownList Ddl, DataTable dt, string datatext, string datavalue)
    //{
    //    try
    //    {

    //        if (dt.Rows.Count > 0)
    //        {
    //            Ddl.DataSource = dt;
    //            Ddl.DataTextField = datatext;
    //            Ddl.DataValueField = datavalue;
    //            Ddl.DataBind();

    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //}
    //public void FillDropDownList_2(DropDownList Ddl, string Storeprocedure, string datatext, string datavalue, List<string> Pname, List<string> PValue, int cnt)
    //{
    //    try
    //    {
    //        con = new SqlConnection();
    //        con = Connect();
    //        cmd = new SqlCommand();
    //        cmd.Connection = con;
    //        cmd.CommandType = CommandType.StoredProcedure;
    //        cmd.CommandText = Storeprocedure;
    //        for (int i = 0; i <= cnt; i++)
    //        {
    //            cmd.Parameters.AddWithValue(Pname[i], PValue[i]);
    //        }
    //        cmd.ExecuteNonQuery();
    //        Da = new SqlDataAdapter();
    //        Ds = new DataSet();
    //        DtTable = new DataTable();
    //        Da.SelectCommand = cmd;
    //        Da.Fill(Ds);
    //        DtTable = Ds.Tables[0];
    //        if (DtTable.Rows.Count > 0)
    //        {
    //            Ddl.DataSource = DtTable;
    //            Ddl.DataTextField = datatext;
    //            Ddl.DataValueField = datavalue;
    //            Ddl.DataBind();

    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //      //  Utilily.UserMessage(ex.Message);
    //    }
    //    finally
    //    {
    //        Close();
    //    }

    //}
    //public bool CheckDuplicatewithCriteria(string TableName, string ColumnName, string ColumnValue, string Criteria)
    //{
    //    try
    //    {
    //        bool flag;
    //        flag = false;
    //        strqry = "Select " + ColumnName + " from " + TableName + " where " + ColumnName + "='" + ColumnValue + "' and " + Criteria + "";

    //        con = Connect();
    //        cmd = new SqlCommand();
    //        cmd.Connection = con;
    //        cmd.CommandType = CommandType.Text;
    //        cmd.CommandText = strqry;
    //        cmd.ExecuteNonQuery();
    //        Da = new SqlDataAdapter();
    //        Ds = new DataSet();
    //        DtTable = new DataTable();
    //        Da.SelectCommand = cmd;
    //        Da.Fill(Ds);
    //        DtTable = Ds.Tables[0];
    //        if (DtTable.Rows.Count > 0)
    //        {
    //            flag = true;

    //        }
    //        return flag;
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //    finally
    //    {
    //        Close();
    //    }
    //}
    //public bool CheckDuplicatewithoutCriteria(string TableName, string ColumnName, string ColumnValue)
    //{
    //    try
    //    {
    //        bool flag;
    //        flag = false;
    //        strqry = "Select " + ColumnName + " from " + TableName + " where  " + ColumnName + "='" + ColumnValue + "'";

    //        con = Connect();
    //        cmd = new SqlCommand();
    //        cmd.Connection = con;
    //        cmd.CommandType = CommandType.Text;
    //        cmd.CommandText = strqry;
    //        cmd.ExecuteNonQuery();
    //        Da = new SqlDataAdapter();
    //        Ds = new DataSet();
    //        DtTable = new DataTable();
    //        Da.SelectCommand = cmd;
    //        Da.Fill(Ds);
    //        DtTable = Ds.Tables[0];
    //        if (DtTable.Rows.Count > 0)
    //        {
    //            flag = true;

    //        }
    //        return flag;
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //    finally
    //    {
    //        Close();
    //    }
    //}
    //public DateTime GetDatetoString(string yourDateString)
    //{
    //    try
    //    {
    //        DateTime mdate = new DateTime();
    //        if (yourDateString != "")
    //        {
    //            mdate = DateTime.ParseExact(yourDateString, "dd/MM/yyyy", null);
    //        }
    //        return mdate;
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //}
    //private string ConvertSortDirection(SortDirection sortDirection)
    //{
    //    try
    //    {
    //        string newSortDirection = String.Empty;

    //        switch (sortDirection)
    //        {
    //            case SortDirection.Ascending:
    //                newSortDirection = "ASC";
    //                break;

    //            case SortDirection.Descending:
    //                newSortDirection = "DESC";
    //                break;
    //        }

    //        return newSortDirection;
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //}
    //public void GetGridSorting(GridView GrdView, string strqry, SortDirection SortDirection, string SortExpression)
    //{
    //    try
    //    {
    //        con = Connect();
    //        cmd = new SqlCommand();
    //        cmd.Connection = con;
    //        cmd.CommandType = CommandType.Text;
    //        cmd.CommandText = strqry;
    //        cmd.ExecuteNonQuery();
    //        Da = new SqlDataAdapter();
    //        Ds = new DataSet();
    //        DtTable = new DataTable();
    //        Da.SelectCommand = cmd;
    //        Da.Fill(Ds);
    //        DtTable = Ds.Tables[0];
    //        if (DtTable.Rows.Count > 0)
    //        {
    //            DataView dataView = new DataView(DtTable);
    //            dataView.Sort = SortExpression + " " + ConvertSortDirection(SortDirection);
    //            GrdView.DataSource = dataView;
    //            GrdView.DataBind();

    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //    finally
    //    {
    //        Close();
    //    }

    //}
    //public void ExecuteCmd(string Cmdstr)
    //{
    //    try
    //    {
    //        //Openconnection();
    //        //Cmd = new SqlCommand(Cmdstr, Con);
    //        //Cmd.ExecuteNonQuery();
    //        //Closeconnection();
    //        con = new SqlConnection();
    //        con = Connect();
    //        cmd = new SqlCommand();
    //        cmd.Connection = con;
    //        cmd.CommandType = CommandType.Text;
    //        cmd.CommandText = Cmdstr;
    //        cmd.ExecuteNonQuery();

    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //    finally
    //    {
    //        Close();
    //    }
    //}
    //public string GetIdByName(string NamecolmName, string NamecolmValue, string TblName, string IdColmnName, string Criteria)
    //{
    //    try
    //    {
    //        con = Connect();
    //        cmd = new SqlCommand();
    //        cmd.Connection = con;
    //        cmd.CommandType = CommandType.Text;
    //        if (Criteria != "")
    //        {
    //            cmd.CommandText = "Select " + IdColmnName + " as Column1 from " + TblName + " where " + NamecolmName + "='" + NamecolmValue + "' and " + Criteria + "";
    //        }
    //        else
    //        {
    //            cmd.CommandText = "Select " + IdColmnName + " as Column1 from " + TblName + " where " + NamecolmName + "='" + NamecolmValue + "' ";
    //        }
    //        cmd.ExecuteNonQuery();
    //        Da = new SqlDataAdapter();
    //        Ds = new DataSet();
    //        DtTable = new DataTable();
    //        Da.SelectCommand = cmd;
    //        Da.Fill(Ds);
    //        DtTable = Ds.Tables[0];
    //        if (DtTable.Rows.Count > 0)
    //        {
    //            strqry = DtTable.Rows[0]["Column1"].ToString();

    //        }
    //        else { strqry = "0"; }

    //        return strqry;
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //    finally
    //    {
    //        Close();
    //    }
    //}
    //public string GetNameById(string NamecolmName, string IdcolmValue, string TblName, string IdColmnName, string Criteria)
    //{
    //    try
    //    {
    //        con = Connect();
    //        cmd = new SqlCommand();
    //        cmd.Connection = con;
    //        cmd.CommandType = CommandType.Text;
    //        if (Criteria != "")
    //        {
    //            cmd.CommandText = "Select " + NamecolmName + " as Column1 from " + TblName + " where " + IdColmnName + "=" + IdcolmValue + " and " + Criteria + "";
    //        }
    //        else
    //        {
    //            cmd.CommandText = "Select " + NamecolmName + " as Column1 from " + TblName + " where " + IdColmnName + "=" + IdcolmValue + " ";
    //        }
    //        cmd.ExecuteNonQuery();
    //        Da = new SqlDataAdapter();
    //        Ds = new DataSet();
    //        DtTable = new DataTable();
    //        Da.SelectCommand = cmd;
    //        Da.Fill(Ds);
    //        DtTable = Ds.Tables[0];
    //        if (DtTable.Rows.Count > 0)
    //        {
    //            strqry = DtTable.Rows[0]["Column1"].ToString();

    //        }
    //        else { strqry = "0"; }

    //        return strqry;
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //    finally
    //    {
    //        Close();
    //    }
    //}
    //public static List<T> ConvertDataTable<T>(DataTable dt)
    //{
    //    List<T> data = new List<T>();
    //    foreach (DataRow row in dt.Rows)
    //    {
    //        T item = GetItem<T>(row);
    //        data.Add(item);
    //    }
    //    return data;
    //}
    //public static T GetItem<T>(DataRow dr)
    //{
    //    Type temp = typeof(T);
    //    T obj = Activator.CreateInstance<T>();

    //    foreach (DataColumn column in dr.Table.Columns)
    //    {
    //        foreach (PropertyInfo pro in temp.GetProperties())
    //        {
    //            if (pro.Name == column.ColumnName)
    //                pro.SetValue(obj, dr[column.ColumnName], null);
    //            else
    //                continue;
    //        }
    //    }
    //    return obj;
    //}

    #region PgSql Functions
    public NpgsqlConnection pgconnection()
    {
        using (NpgsqlConnection con = new NpgsqlConnection())
        {
            con.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings.ToString();

            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
            con.Open();
            return con;
        }
    }

    public DataTable pg_GetDatatableStoreprocedure(string FnName, List<string> Pname, List<object> PValue, int cnt)
    {
        try
        {
            NpgsqlConnection con = pgconnection();

            NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = FnName;
            for (int i = 0; i <= cnt; i++)
            {
                cmd.Parameters.AddWithValue(Pname[i], PValue[i]);
            }
            cmd.ExecuteNonQuery();
            NpgsqlDataAdapter Da = new NpgsqlDataAdapter();
            DtTable = new DataTable();
            Da.SelectCommand = cmd;
            Da.Fill(DtTable);
            // return DtTable;
        }
        catch (Exception ex)
        {
            //  Utilily.UserMessage(ex.Message);
            //  Utilily.LogException(ex, "Error Occured While Calling " + FnName);
            //   Utilily.ManageUserLog("0", ex.Message, "Utility.svc/");
        }
        finally
        {
            Close();
        }
        return DtTable;
    }

    #endregion

}
