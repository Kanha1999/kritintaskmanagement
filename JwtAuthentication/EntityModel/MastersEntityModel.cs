﻿//Author :Kishan Mahajan   
//Date :
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;   
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Npgsql;
using static JwtAuthentication.ViewModels.MasterViewModel;
using System.Data;
using Newtonsoft.Json;
using System.Net.Mail;
using System.Globalization;

namespace JwtAuthentication.EntityModel
{
   

    //[Authorize]
    //[Route("V1/[controller]")]
    //[ApiController]
    //[EnableCors("AllowFromAll")]
    public class MastersEntityModel
    {






        //Project Random quote
        public Reply AddQuote(GetQuote model) // for update payment status at delevery time
        {
            Reply res = new Reply();
            try
            {
                DataConnection datacon = new DataConnection();
                NpgsqlConnection con = datacon.pgconnect();
                NpgsqlCommand cmd;
                cmd = new NpgsqlCommand
                {
                    Connection = con
                };
                cmd.Parameters.Add(new NpgsqlParameter { ParameterName = "contentt", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar, Value = model.Quote });//userid
                cmd.CommandText = "masters.fn_add_qt";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                int responseid = Convert.ToInt32(cmd.ExecuteScalar());
                if (responseid == 0)
                {
                    res.Response = "quote Update Sucsessfully";
                }

                else if (responseid == -1)
                {
                    res.Response = "quote AllReady Exit";
                }
                else
                {
                    res.Response = "quote Addded Sucsessfully :  "+ responseid;
                }
                con.Close();
                res.Status = 200;
                res.Message = "Ok";
                return res;
            }
            catch (Exception ex) // AddUpdateDepartment
            {
                string error = ex.Message + ',' + "AddQuote";
                res.Status = 500;
                res.Message = error;
                res.Response = error;
                return res;
            }
        }//End AddUpdateDepartment

        public Reply ShowQuote()//for show  Quote 
        {
            Reply res = new Reply();
            try
            {
                DataConnection datacon = new DataConnection();
                NpgsqlConnection con = datacon.pgconnect();
                NpgsqlCommand cmd;
                cmd = new NpgsqlCommand();
                cmd.Connection = con;
                //cmd.Parameters.Add(new NpgsqlParameter { ParameterName = "vcreatedby", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Bigint, Value = createdby });
                //cmd.Parameters.Add(new NpgsqlParameter { ParameterName = "vemp_id", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Bigint, Value = emp_id });
                cmd.CommandText = "masters.fn_show_qt";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                NpgsqlDataAdapter ad = new NpgsqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                DataTable DT = new DataTable();
                ad.Fill(ds);
                DT = ds.Tables[0];
                List<ShowQuote> quotelist = new List<ShowQuote>();
                if (DT.Rows.Count > 0)
                {
                    for (int i = 0; i < DT.Rows.Count; i++)
                    {
                        quotelist.Add(new ShowQuote
                        {
                            //payroll_id = Convert.ToInt32((DT.Rows[i]["payroll_id"].ToString())),
                            Quote = (DT.Rows[i]["contentt"].ToString())
                        });
                    }
                }
                con.Close();
                res.Status = 200;
                res.Message = "Ok";
                res.Response = quotelist;
                return res;
            }
            catch (Exception ex)//Show
            {
                string error = ex.Message + "Masters.EntityModel.CS/ShowQuote";
                res.Status = 500;
                res.Message = error;
                res.Response = error;
                return res;
            }
        }
        //End Project Random quote









    // Project task management
        public Reply AddEmp(Emp model) // adding new employee
        {
            Reply res = new Reply();
            try
            {
                DataConnection datacon = new DataConnection();
                NpgsqlConnection con = datacon.pgconnect();
                NpgsqlCommand cmd;
                cmd = new NpgsqlCommand();
                cmd.Connection = con;
                cmd.Parameters.Add(new NpgsqlParameter { ParameterName = "ename", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar, Value = model.EmpName });//userid
                cmd.CommandText = "taskm.fn_add_emp";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                int responseid = Convert.ToInt32(cmd.ExecuteScalar());
                if (responseid == -1)
                {
                    res.Response = "emp AllReady Exist";
                }
                else
                {
                    res.Response = "emp Addded Sucsessfully :  " + responseid;
                }
                con.Close();
                res.Status = 200;
                res.Message = "Ok";
                return res;
            }
            catch (Exception ex) 
            {
                string error = ex.Message + ',' + "AddEmp";
                res.Status = 500;
                res.Message = error;
                res.Response = error;
                return res;
            }
        }//End AddEmp
        
        public Reply ShowAllEmp()//for Show All Emp in db
        {
            Reply res = new Reply();
            try
            {
                DataConnection datacon = new DataConnection();
                NpgsqlConnection con = datacon.pgconnect();
                NpgsqlCommand cmd;
                cmd = new NpgsqlCommand();
                cmd.Connection = con;
                cmd.CommandText = "taskm.fn_show_all_emp";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                NpgsqlDataAdapter ad = new NpgsqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                DataTable DT = new DataTable();
                ad.Fill(ds);
                DT = ds.Tables[0];
                List<Emp> quotelist = new List<Emp>();
                if (DT.Rows.Count > 0)
                {
                    for (int i = 0; i < DT.Rows.Count; i++)
                    {
                        quotelist.Add(new Emp
                        {
                            EmpId = Convert.ToInt32((DT.Rows[i]["eid"].ToString())),
                            EmpName = (DT.Rows[i]["ename"].ToString())
                        });
                    }
                }
                con.Close();
                res.Status = 200;
                res.Message = "Ok";
                res.Response = quotelist;
                return res;
            }
            catch (Exception ex)
            {
                string error = ex.Message + "Masters.EntityModel.CS/ShowAllEmp";
                res.Status = 500;
                res.Message = error;
                res.Response = error;
                return res;
            }
        }
        public Reply AddTaskAdmin(TaskD model) // for  Add task and assign to emp
        {
            Reply res = new Reply();
            DataConnection datacon = new DataConnection();
            NpgsqlConnection con = datacon.pgconnect();
            NpgsqlCommand cmd;
            try
            {    
                cmd = new NpgsqlCommand();
                cmd.Connection = con;
                DateTimeFormatInfo fmt = new CultureInfo("fr-fr").DateTimeFormat;
                //string format = "yyyy.MM.dd HH:mm:ss:ffff";
                //DateTime deadlineTime = DateTime.ParseExact(model.deadline, format,CultureInfo.InvariantCulture);
                DateTimeOffset deadlineTime = DateTimeOffset.Parse(model.Deadline, fmt);
                cmd.Parameters.Add(new NpgsqlParameter { ParameterName = "vtask", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar, Value = model.Task });
                cmd.Parameters.Add(new NpgsqlParameter { ParameterName = "vresponsible", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = model.Responsible });
                cmd.Parameters.Add(new NpgsqlParameter { ParameterName = "vdeadline", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.TimestampTz, Value = deadlineTime });
                cmd.Parameters.Add(new NpgsqlParameter { ParameterName = "vcategory", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar, Value = model.Category });
                cmd.Parameters.Add(new NpgsqlParameter { ParameterName = "vpriority", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = model.PriorityId });
                cmd.CommandText = "taskm.fn_add_task_admin";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                int responseid1 = Convert.ToInt32(cmd.ExecuteScalar());
                for (int i = 0; i < model.Action.Count; i++)
                {
                    cmd = new NpgsqlCommand();
                    cmd.Connection = con;
                    cmd.Parameters.Add(new NpgsqlParameter { ParameterName = "vtaskid", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = responseid1 });
                    cmd.Parameters.Add(new NpgsqlParameter { ParameterName = "vaction", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar, Value = model.Action[i] });
                    cmd.CommandText = "taskm.fn_add_actions";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    int responseid2 = Convert.ToInt32(cmd.ExecuteScalar());
                    if (responseid2 == -1)
                    {
                        res.Response = "task does not exist";
                    }
                    else
                    {
                        res.Response = "action:  :" + responseid2;
                    }
                }
                
                for (int i = 0; i < model.SupportedBy.Count; i++)
                {
                    cmd = new NpgsqlCommand();
                    cmd.Connection = con;
                    cmd.Parameters.Add(new NpgsqlParameter { ParameterName = "vtid", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = responseid1 });
                    cmd.Parameters.Add(new NpgsqlParameter { ParameterName = "veid", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = model.SupportedBy[i] });
                    cmd.CommandText = "taskm.fn_add_support";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    int responseid3 = Convert.ToInt32(cmd.ExecuteScalar());
                    if (responseid3 == -1)
                    {
                        res.Response += "task does not exist";
                    }
                    else
                    {
                        res.Response += "Support :  :" + responseid3;
                    }
                }
                if (responseid1 == -1)
                {
                    res.Response += "task AllReady Exist";
                }
                else
                {
                    res.Response += "task Addded Sucsessfully :  " + responseid1;
                }
                con.Close();
                res.Status = 200;
                res.Message = "Ok";
                return res;
            }
            catch (Exception ex)
            {
                string error = ex.Message + ',' + "AddTask";
                res.Status = 500;
                res.Message = error;
                res.Response = error;
                return res;
            }
 
        }//End AddTaskAdmin

        public Reply AddTaskEmp(TaskD model) // change task status and add notes also time compaired with deadline to add late in status in task , status 0:in progress  1:completed
        {
            Reply res = new Reply();
            DataConnection datacon = new DataConnection();
            NpgsqlConnection con = datacon.pgconnect();
            NpgsqlCommand cmd;
            try
            {
                
                cmd = new NpgsqlCommand();
                cmd.Connection = con;
                //string format = "yyyy.MM.dd HH:mm:ss:ffff";
                //DateTime deadlineTime = DateTime.ParseExact(model.deadline, format,CultureInfo.InvariantCulture);
                cmd.Parameters.Add(new NpgsqlParameter { ParameterName = "vtask_id", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = model.TaskId });
                cmd.CommandText = "taskm.fn_show_task_deadline";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                DateTime dlTime= Convert.ToDateTime(cmd.ExecuteScalar());
                if (model.StatusId == 0)
                { model.StatusId = 2; }
                else
                { model.StatusId = 4; }
                DateTime tempsubmittedOn = DateTime.Now;
                int com= DateTime.Compare(dlTime, tempsubmittedOn);
                if (com < 0)
                {
                    model.StatusId += 1;
                }

                cmd = new NpgsqlCommand();
                cmd.Connection = con;

                cmd.Parameters.Add(new NpgsqlParameter { ParameterName = "vtask_id", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = model.TaskId });
                cmd.Parameters.Add(new NpgsqlParameter { ParameterName = "vstatus", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = model.StatusId });
                cmd.Parameters.Add(new NpgsqlParameter { ParameterName = "vsubmitted_on", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.TimestampTz, Value = tempsubmittedOn });
                cmd.Parameters.Add(new NpgsqlParameter { ParameterName = "vnotes", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar, Value = model.Notes });
                cmd.CommandText = "taskm.fn_add_task_emp";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                int responseid1 = Convert.ToInt32(cmd.ExecuteScalar());
                if (responseid1 == -1)
                {
                    res.Response = "task does not Exist";
                }
                else
                {
                    res.Response = "task modified Sucsessfully :  " + responseid1;
                }
                con.Close();
                res.Status = 200;
                res.Message = "Ok";
                return res;
            }
            catch (Exception ex)
            {
                string error = ex.Message + ',' + "ModifyTask";
                res.Status = 500;
                res.Message = error;
                res.Response = error;
                return res;
            }

        }//End AddTaskEmp



        public Reply ShowAllTask() // for showing all task
        {
            Reply res = new Reply();
            try
            {
                DataConnection datacon = new DataConnection();
                NpgsqlConnection con = datacon.pgconnect();
                NpgsqlCommand cmd;
                cmd = new NpgsqlCommand();
                cmd.Connection = con;
                cmd.CommandText = "taskm.fn_show_task_all";                
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                NpgsqlDataAdapter ad = new NpgsqlDataAdapter(cmd);    
                DataSet ds = new DataSet();
                DataSet ds1 = new DataSet();
                DataSet ds2 = new DataSet();
                DataTable DT = new DataTable();   
                ad.Fill(ds);   
                DT = ds.Tables[0];   
                List<TaskD> Tasklist = new List<TaskD>();
                if (DT.Rows.Count > 0)
                {
                    for (int i = 0; i < DT.Rows.Count; i++)
                    {
                        // get supported_by and tasks from different functions
                        int taskId = Convert.ToInt32((DT.Rows[i]["Vtask_id"].ToString()));
                        NpgsqlCommand cmd1;
                        cmd1 = new NpgsqlCommand();
                        cmd1.Connection = con;
                        cmd1.Parameters.Add(new NpgsqlParameter { ParameterName = "id", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = taskId });
                        cmd1.CommandText = "taskm.fn_show_supported_by_task_id";
                        cmd1.CommandType = System.Data.CommandType.StoredProcedure;
                        NpgsqlDataAdapter ad1 = new NpgsqlDataAdapter(cmd1);
                        DataTable DT1 = new DataTable();
                        ad1.Fill(ds1);
                        DT1 = ds1.Tables[0];

                        List<string> tempSupportList = new List<string>();

                        if (DT1.Rows.Count > 0)
                        {
                            for (int j = 0; j < DT1.Rows.Count; j++)
                            {
                                tempSupportList.Add(DT1.Rows[j]["vsupported_by"].ToString());
                            }
                        }
                        NpgsqlCommand cmd2;
                        cmd2 = new NpgsqlCommand();
                        cmd2.Connection = con;
                        cmd2.Parameters.Add(new NpgsqlParameter { ParameterName = "id", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = taskId });
                        cmd2.CommandText = "taskm.fn_show_action_details_task_id";
                        cmd2.CommandType = System.Data.CommandType.StoredProcedure;
                        NpgsqlDataAdapter ad2 = new NpgsqlDataAdapter(cmd2);
                        DataTable DT2 = new DataTable();
                        ad2.Fill(ds2);
                        DT2 = ds2.Tables[0];
                        List<string> tempActionList = new List<string>();
                        if (DT2.Rows.Count > 0)
                        {
                            for (int j = 0; j < DT2.Rows.Count; j++)
                            {
                                tempActionList.Add(DT2.Rows[j]["vaction"].ToString());
                            }
                        }
                        Tasklist.Add(new TaskD
                        {
                            TaskId = Convert.ToInt32((DT.Rows[i]["Vtask_id"].ToString())),
                            Task = (DT.Rows[i]["vtask"].ToString()),
                            ResponsibleName = (DT.Rows[i]["vresponsible_name"].ToString()),
                            CreatedAt = Convert.ToDateTime(DT.Rows[i]["vcreated_at"]),
                            Deadline = (DT.Rows[i]["vdeadline"].ToString()),
                            Category = (DT.Rows[i]["vcat_name"].ToString()),
                            Priority = (DT.Rows[i]["vpriority"].ToString()),
                            Status = (DT.Rows[i]["vstatus_type"].ToString()),
                            SubmittedOn = (DT.Rows[i]["vsubmitted_on"].ToString()),
                            Notes = (DT.Rows[i]["vnotes"].ToString()),
                            SupportedByName= tempSupportList,
                            Action= tempActionList
                        });
                        DT1.Clear();
                        DT2.Clear();
                    }
                }

                con.Close();
                res.Status = 200;
                res.Message = "Ok";
                res.Response = Tasklist;
                return res;
            }
            catch (Exception ex)//Show all task 
            {
                string error = ex.Message + "";
                res.Status = 500;
                res.Message = error;
                res.Response = error;
                return res;
            }
        }
        public Reply ShowByTaskId(int taskId)//show task for given task_id
        {
            Reply res = new Reply();
            try
            {
                DataConnection datacon = new DataConnection();
                NpgsqlConnection con = datacon.pgconnect();
                NpgsqlCommand cmd;
                cmd = new NpgsqlCommand();
                cmd.Connection = con;
                cmd.Parameters.Add(new NpgsqlParameter { ParameterName = "id", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = taskId });
                cmd.CommandText = "taskm.fn_show_task_task_id";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                NpgsqlDataAdapter ad = new NpgsqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                DataSet ds1 = new DataSet();
                DataSet ds2 = new DataSet();
                DataTable DT = new DataTable();
                ad.Fill(ds);
                DT = ds.Tables[0];
                List<TaskD> Tasklist = new List<TaskD>();
                NpgsqlCommand cmd1;
                cmd1 = new NpgsqlCommand();
                cmd1.Connection = con;
                cmd1.Parameters.Add(new NpgsqlParameter { ParameterName = "id", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = taskId });
                cmd1.CommandText = "taskm.fn_show_supported_by_task_id";
                cmd1.CommandType = System.Data.CommandType.StoredProcedure;
                NpgsqlDataAdapter ad1 = new NpgsqlDataAdapter(cmd1);
                DataTable DT1 = new DataTable();
                ad1.Fill(ds1);
                DT1 = ds1.Tables[0];
                List<string> tempSupportList = new List<string>();
                if (DT1.Rows.Count > 0)
                {
                    for (int j = 0; j < DT1.Rows.Count; j++)
                    {
                        tempSupportList.Add(DT1.Rows[j]["vsupported_by"].ToString());
                    }
                }
                NpgsqlCommand cmd2;
                cmd2 = new NpgsqlCommand();
                cmd2.Connection = con;
                cmd2.Parameters.Add(new NpgsqlParameter { ParameterName = "id", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = taskId });
                cmd2.CommandText = "taskm.fn_show_action_details_task_id";
                cmd2.CommandType = System.Data.CommandType.StoredProcedure;
                NpgsqlDataAdapter ad2 = new NpgsqlDataAdapter(cmd2);
                DataTable DT2 = new DataTable();
                ad2.Fill(ds2);
                DT2 = ds2.Tables[0];
                List<string> tempActionList = new List<string>();
                if (DT2.Rows.Count > 0)
                {
                    for (int j = 0; j < DT2.Rows.Count; j++)
                    {
                        tempActionList.Add(DT2.Rows[j]["vaction"].ToString());
                    }
                }
                Tasklist.Add(new TaskD
                {
                    TaskId = Convert.ToInt32((DT.Rows[0]["Vtask_id"].ToString())),
                    Task = (DT.Rows[0]["vtask"].ToString()),
                    ResponsibleName = (DT.Rows[0]["vresponsible_name"].ToString()),
                    CreatedAt = Convert.ToDateTime(DT.Rows[0]["vcreated_at"]),
                    Deadline = (DT.Rows[0]["vdeadline"].ToString()),
                    Category = (DT.Rows[0]["vcat_name"].ToString()),
                    Priority = (DT.Rows[0]["vpriority"].ToString()),
                    Status = (DT.Rows[0]["vstatus_type"].ToString()),
                    SubmittedOn = (DT.Rows[0]["vsubmitted_on"].ToString()),
                    Notes = (DT.Rows[0]["vnotes"].ToString()),
                    SupportedByName = tempSupportList,
                    Action = tempActionList
                });
                con.Close();
                res.Status = 200;
                res.Message = "Ok";
                res.Response = Tasklist;
                return res;
            }
            catch (Exception ex)//ShowTask
            {
                string error = ex.Message + "";
                res.Status = 500;
                res.Message = error;
                res.Response = error;
                return res;
            }
        }
        public Reply ShowByEmpId(int empId)//show task for given emp_id
        {
            Reply res = new Reply();
            try
            {
                DataConnection datacon = new DataConnection();
                NpgsqlConnection con = datacon.pgconnect();
                NpgsqlCommand cmd;
                cmd = new NpgsqlCommand();
                cmd.Connection = con;
                cmd.Parameters.Add(new NpgsqlParameter { ParameterName = "id", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = empId });
                cmd.CommandText = "taskm.fn_show_task_emp";                
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                NpgsqlDataAdapter ad = new NpgsqlDataAdapter(cmd);    
                DataSet ds = new DataSet();
                DataSet ds1 = new DataSet();
                DataSet ds2 = new DataSet();
                DataTable DT = new DataTable();   
                ad.Fill(ds);   
                DT = ds.Tables[0];   
                List<TaskD> Tasklist = new List<TaskD>();
                if (DT.Rows.Count > 0)
                {
                    for (int i = 0; i < DT.Rows.Count; i++)
                    {
                        int taskId = Convert.ToInt32((DT.Rows[i]["Vtask_id"].ToString()));
                        NpgsqlCommand cmd1;
                        cmd1 = new NpgsqlCommand();
                        cmd1.Connection = con;
                        cmd1.Parameters.Add(new NpgsqlParameter { ParameterName = "id", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = taskId });
                        cmd1.CommandText = "taskm.fn_show_supported_by_task_id";
                        cmd1.CommandType = System.Data.CommandType.StoredProcedure;
                        NpgsqlDataAdapter ad1 = new NpgsqlDataAdapter(cmd1);
                        DataTable DT1 = new DataTable();
                        ad1.Fill(ds1);
                        DT1 = ds1.Tables[0];

                        List<string> tempSupportList = new List<string>();

                        if (DT1.Rows.Count > 0)
                        {
                            for (int j = 0; j < DT1.Rows.Count; j++)
                            {
                                tempSupportList.Add(DT1.Rows[j]["vsupported_by"].ToString());
                            }
                        }
                        NpgsqlCommand cmd2;
                        cmd2 = new NpgsqlCommand();
                        cmd2.Connection = con;
                        cmd2.Parameters.Add(new NpgsqlParameter { ParameterName = "id", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = taskId });
                        cmd2.CommandText = "taskm.fn_show_action_details_task_id";
                        cmd2.CommandType = System.Data.CommandType.StoredProcedure;
                        NpgsqlDataAdapter ad2 = new NpgsqlDataAdapter(cmd2);
                        DataTable DT2 = new DataTable();
                        ad2.Fill(ds2);
                        DT2 = ds2.Tables[0];
                        List<string> tempActionList = new List<string>();
                        if (DT2.Rows.Count > 0)
                        {
                            for (int j = 0; j < DT2.Rows.Count; j++)
                            {
                                tempActionList.Add(DT2.Rows[j]["vaction"].ToString());
                            }
                        }
                        Tasklist.Add(new TaskD
                        {
                            TaskId = Convert.ToInt32((DT.Rows[i]["Vtask_id"].ToString())),
                            Task = (DT.Rows[i]["vtask"].ToString()),
                            ResponsibleName = (DT.Rows[i]["vresponsible_name"].ToString()),
                            CreatedAt = Convert.ToDateTime(DT.Rows[i]["vcreated_at"]),
                            Deadline = (DT.Rows[i]["vdeadline"].ToString()),
                            Category = (DT.Rows[i]["vcat_name"].ToString()),
                            Priority = (DT.Rows[i]["vpriority"].ToString()),
                            Status = (DT.Rows[i]["vstatus_type"].ToString()),
                            SubmittedOn = (DT.Rows[i]["vsubmitted_on"].ToString()),
                            Notes = (DT.Rows[i]["vnotes"].ToString()),
                            SupportedByName= tempSupportList,
                            Action= tempActionList
                        });
                        DT1.Clear();
                        DT2.Clear();
                    }
                }

                con.Close();
                res.Status = 200;
                res.Message = "Ok";
                res.Response = Tasklist;
                return res;
            }
            catch (Exception ex)//Show
            {
                string error = ex.Message + "";
                res.Status = 500;
                res.Message = error;
                res.Response = error;
                return res;
            }
        }
        public Reply ShowByEmp(string empName)//show task for given emp_name
        {
            Reply res = new Reply();
            try
            {
                DataConnection datacon = new DataConnection();
                NpgsqlConnection con = datacon.pgconnect();
                NpgsqlCommand cmd;
                cmd = new NpgsqlCommand();
                cmd.Connection = con;
                cmd.Parameters.Add(new NpgsqlParameter { ParameterName = "vname", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar, Value = empName });
                cmd.CommandText = "taskm.fn_show_task_emp";                
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                NpgsqlDataAdapter ad = new NpgsqlDataAdapter(cmd);    
                DataSet ds = new DataSet();
                DataSet ds1 = new DataSet();
                DataSet ds2 = new DataSet();
                DataTable DT = new DataTable();   
                ad.Fill(ds);   
                DT = ds.Tables[0];   
                List<TaskD> Tasklist = new List<TaskD>();
                if (DT.Rows.Count > 0)
                {
                    for (int i = 0; i < DT.Rows.Count; i++)
                    {
                        int taskId = Convert.ToInt32((DT.Rows[i]["Vtask_id"].ToString()));
                        NpgsqlCommand cmd1;
                        cmd1 = new NpgsqlCommand();
                        cmd1.Connection = con;
                        cmd1.Parameters.Add(new NpgsqlParameter { ParameterName = "id", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = taskId });
                        cmd1.CommandText = "taskm.fn_show_supported_by_task_id";
                        cmd1.CommandType = System.Data.CommandType.StoredProcedure;
                        NpgsqlDataAdapter ad1 = new NpgsqlDataAdapter(cmd1);
                        DataTable DT1 = new DataTable();
                        ad1.Fill(ds1);
                        DT1 = ds1.Tables[0];

                        List<string> tempSupportList = new List<string>();

                        if (DT1.Rows.Count > 0)
                        {
                            for (int j = 0; j < DT1.Rows.Count; j++)
                            {
                                tempSupportList.Add(DT1.Rows[j]["vsupported_by"].ToString());
                            }
                        }
                        NpgsqlCommand cmd2;
                        cmd2 = new NpgsqlCommand();
                        cmd2.Connection = con;
                        cmd2.Parameters.Add(new NpgsqlParameter { ParameterName = "id", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = taskId });
                        cmd2.CommandText = "taskm.fn_show_action_details_task_id";
                        cmd2.CommandType = System.Data.CommandType.StoredProcedure;
                        NpgsqlDataAdapter ad2 = new NpgsqlDataAdapter(cmd2);
                        DataTable DT2 = new DataTable();
                        ad2.Fill(ds2);
                        DT2 = ds2.Tables[0];
                        List<string> tempActionList = new List<string>();
                        if (DT2.Rows.Count > 0)
                        {
                            for (int j = 0; j < DT2.Rows.Count; j++)
                            {
                                tempActionList.Add(DT2.Rows[j]["vaction"].ToString());
                            }
                        }
                        Tasklist.Add(new TaskD
                        {
                            TaskId = Convert.ToInt32((DT.Rows[i]["Vtask_id"].ToString())),
                            Task = (DT.Rows[i]["vtask"].ToString()),
                            ResponsibleName = (DT.Rows[i]["vresponsible_name"].ToString()),
                            CreatedAt = Convert.ToDateTime(DT.Rows[i]["vcreated_at"]),
                            Deadline = (DT.Rows[i]["vdeadline"].ToString()),
                            Category = (DT.Rows[i]["vcat_name"].ToString()),
                            Priority = (DT.Rows[i]["vpriority"].ToString()),
                            Status = (DT.Rows[i]["vstatus_type"].ToString()),
                            SubmittedOn = (DT.Rows[i]["vsubmitted_on"].ToString()),
                            Notes = (DT.Rows[i]["vnotes"].ToString()),
                            SupportedByName= tempSupportList,
                            Action= tempActionList
                        });
                        DT1.Clear();
                        DT2.Clear();
                    }
                }

                con.Close();
                res.Status = 200;
                res.Message = "Ok";
                res.Response = Tasklist;
                return res;
            }
            catch (Exception ex)//Show
            {
                string error = ex.Message + "";
                res.Status = 500;
                res.Message = error;
                res.Response = error;
                return res;
            }
        }
        
        public Reply ShowByStatusId(int statusId)//show task a/c to given status
        {
            Reply res = new Reply();
            try
            {
                DataConnection datacon = new DataConnection();
                NpgsqlConnection con = datacon.pgconnect();
                NpgsqlCommand cmd;
                cmd = new NpgsqlCommand();
                cmd.Connection = con;
                cmd.Parameters.Add(new NpgsqlParameter { ParameterName = "sid", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = statusId });
                cmd.CommandText = "taskm.fn_show_task_status";                
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                NpgsqlDataAdapter ad = new NpgsqlDataAdapter(cmd);    
                DataSet ds = new DataSet();
                DataSet ds1 = new DataSet();
                DataSet ds2 = new DataSet();
                DataTable DT = new DataTable();   
                ad.Fill(ds);   
                DT = ds.Tables[0];   
                List<TaskD> Tasklist = new List<TaskD>();
                if (DT.Rows.Count > 0)
                {
                    for (int i = 0; i < DT.Rows.Count; i++)
                    {
                        int taskId = Convert.ToInt32((DT.Rows[i]["Vtask_id"].ToString()));
                        NpgsqlCommand cmd1;
                        cmd1 = new NpgsqlCommand();
                        cmd1.Connection = con;
                        cmd1.Parameters.Add(new NpgsqlParameter { ParameterName = "id", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = taskId });
                        cmd1.CommandText = "taskm.fn_show_supported_by_task_id";
                        cmd1.CommandType = System.Data.CommandType.StoredProcedure;
                        NpgsqlDataAdapter ad1 = new NpgsqlDataAdapter(cmd1);
                        DataTable DT1 = new DataTable();
                        ad1.Fill(ds1);
                        DT1 = ds1.Tables[0];

                        List<string> tempSupportList = new List<string>();

                        if (DT1.Rows.Count > 0)
                        {
                            for (int j = 0; j < DT1.Rows.Count; j++)
                            {
                                tempSupportList.Add(DT1.Rows[j]["vsupported_by"].ToString());
                            }
                        }
                        NpgsqlCommand cmd2;
                        cmd2 = new NpgsqlCommand();
                        cmd2.Connection = con;
                        cmd2.Parameters.Add(new NpgsqlParameter { ParameterName = "id", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = taskId });
                        cmd2.CommandText = "taskm.fn_show_action_details_task_id";
                        cmd2.CommandType = System.Data.CommandType.StoredProcedure;
                        NpgsqlDataAdapter ad2 = new NpgsqlDataAdapter(cmd2);
                        DataTable DT2 = new DataTable();
                        ad2.Fill(ds2);
                        DT2 = ds2.Tables[0];
                        List<string> tempActionList = new List<string>();
                        if (DT2.Rows.Count > 0)
                        {
                            for (int j = 0; j < DT2.Rows.Count; j++)
                            {
                                tempActionList.Add(DT2.Rows[j]["vaction"].ToString());
                            }
                        }
                        Tasklist.Add(new TaskD
                        {
                            TaskId = Convert.ToInt32((DT.Rows[i]["Vtask_id"].ToString())),
                            Task = (DT.Rows[i]["vtask"].ToString()),
                            ResponsibleName = (DT.Rows[i]["vresponsible_name"].ToString()),
                            CreatedAt = Convert.ToDateTime(DT.Rows[i]["vcreated_at"]),
                            Deadline = (DT.Rows[i]["vdeadline"].ToString()),
                            Category = (DT.Rows[i]["vcat_name"].ToString()),
                            Priority = (DT.Rows[i]["vpriority"].ToString()),
                            Status = (DT.Rows[i]["vstatus_type"].ToString()),
                            SubmittedOn = (DT.Rows[i]["vsubmitted_on"].ToString()),
                            Notes = (DT.Rows[i]["vnotes"].ToString()),
                            SupportedByName= tempSupportList,
                            Action= tempActionList
                        });
                        DT1.Clear();
                        DT2.Clear();
                    }
                }

                con.Close();
                res.Status = 200;
                res.Message = "Ok";
                res.Response = Tasklist;
                return res;
            }
            catch (Exception ex)//Show task
            {
                string error = ex.Message + "";
                res.Status = 500;
                res.Message = error;
                res.Response = error;
                return res;
            }
        }

        public Reply ShowByPriorityId(int priorityId)//show task a/c to given priority
        {
            Reply res = new Reply();
            try
            {
                DataConnection datacon = new DataConnection();
                NpgsqlConnection con = datacon.pgconnect();
                NpgsqlCommand cmd;
                cmd = new NpgsqlCommand();
                cmd.Connection = con;
                cmd.Parameters.Add(new NpgsqlParameter { ParameterName = "pid", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = priorityId });
                cmd.CommandText = "taskm.fn_show_task_priority";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                NpgsqlDataAdapter ad = new NpgsqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                DataSet ds1 = new DataSet();
                DataSet ds2 = new DataSet();
                DataTable DT = new DataTable();
                ad.Fill(ds);
                DT = ds.Tables[0];
                List<TaskD> Tasklist = new List<TaskD>();
                if (DT.Rows.Count > 0)
                {
                    for (int i = 0; i < DT.Rows.Count; i++)
                    {
                        int taskId = Convert.ToInt32((DT.Rows[i]["Vtask_id"].ToString()));
                        NpgsqlCommand cmd1;
                        cmd1 = new NpgsqlCommand();
                        cmd1.Connection = con;
                        cmd1.Parameters.Add(new NpgsqlParameter { ParameterName = "id", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = taskId });
                        cmd1.CommandText = "taskm.fn_show_supported_by_task_id";
                        cmd1.CommandType = System.Data.CommandType.StoredProcedure;
                        NpgsqlDataAdapter ad1 = new NpgsqlDataAdapter(cmd1);
                        DataTable DT1 = new DataTable();
                        ad1.Fill(ds1);
                        DT1 = ds1.Tables[0];

                        List<string> tempSupportList = new List<string>();

                        if (DT1.Rows.Count > 0)
                        {
                            for (int j = 0; j < DT1.Rows.Count; j++)
                            {
                                tempSupportList.Add(DT1.Rows[j]["vsupported_by"].ToString());
                            }
                        }
                        NpgsqlCommand cmd2;
                        cmd2 = new NpgsqlCommand();
                        cmd2.Connection = con;
                        cmd2.Parameters.Add(new NpgsqlParameter { ParameterName = "id", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = taskId });
                        cmd2.CommandText = "taskm.fn_show_action_details_task_id";
                        cmd2.CommandType = System.Data.CommandType.StoredProcedure;
                        NpgsqlDataAdapter ad2 = new NpgsqlDataAdapter(cmd2);
                        DataTable DT2 = new DataTable();
                        ad2.Fill(ds2);
                        DT2 = ds2.Tables[0];
                        List<string> tempActionList = new List<string>();
                        if (DT2.Rows.Count > 0)
                        {
                            for (int j = 0; j < DT2.Rows.Count; j++)
                            {
                                tempActionList.Add(DT2.Rows[j]["vaction"].ToString());
                            }
                        }
                        Tasklist.Add(new TaskD
                        {
                            TaskId = Convert.ToInt32((DT.Rows[i]["Vtask_id"].ToString())),
                            Task = (DT.Rows[i]["vtask"].ToString()),
                            ResponsibleName = (DT.Rows[i]["vresponsible_name"].ToString()),
                            CreatedAt = Convert.ToDateTime(DT.Rows[i]["vcreated_at"]),
                            Deadline = (DT.Rows[i]["vdeadline"].ToString()),
                            Category = (DT.Rows[i]["vcat_name"].ToString()),
                            Priority = (DT.Rows[i]["vpriority"].ToString()),
                            Status = (DT.Rows[i]["vstatus_type"].ToString()),
                            SubmittedOn = (DT.Rows[i]["vsubmitted_on"].ToString()),
                            Notes = (DT.Rows[i]["vnotes"].ToString()),
                            SupportedByName = tempSupportList,
                            Action = tempActionList
                        });
                        DT1.Clear();
                        DT2.Clear();
                    }
                }

                con.Close();
                res.Status = 200;
                res.Message = "Ok";
                res.Response = Tasklist;
                return res;
            }
            catch (Exception ex)//Show
            {
                string error = ex.Message + "";
                res.Status = 500;
                res.Message = error;
                res.Response = error;
                return res;
            }
        }
        public Reply ShowBycategoryId(int categoryId)//show task a/c to given catetgory Id
        {
            Reply res = new Reply();
            try
            {
                DataConnection datacon = new DataConnection();
                NpgsqlConnection con = datacon.pgconnect();
                NpgsqlCommand cmd;
                cmd = new NpgsqlCommand();
                cmd.Connection = con;
                cmd.Parameters.Add(new NpgsqlParameter { ParameterName = "cid", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = categoryId });
                cmd.CommandText = "taskm.fn_show_task_cat";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                NpgsqlDataAdapter ad = new NpgsqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                DataSet ds1 = new DataSet();
                DataSet ds2 = new DataSet();
                DataTable DT = new DataTable();
                ad.Fill(ds);
                DT = ds.Tables[0];
                List<TaskD> Tasklist = new List<TaskD>();
                if (DT.Rows.Count > 0)
                {
                    for (int i = 0; i < DT.Rows.Count; i++)
                    {
                        int taskId = Convert.ToInt32((DT.Rows[i]["Vtask_id"].ToString()));
                        NpgsqlCommand cmd1;
                        cmd1 = new NpgsqlCommand();
                        cmd1.Connection = con;
                        cmd1.Parameters.Add(new NpgsqlParameter { ParameterName = "id", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = taskId });
                        cmd1.CommandText = "taskm.fn_show_supported_by_task_id";
                        cmd1.CommandType = System.Data.CommandType.StoredProcedure;
                        NpgsqlDataAdapter ad1 = new NpgsqlDataAdapter(cmd1);
                        DataTable DT1 = new DataTable();
                        ad1.Fill(ds1);
                        DT1 = ds1.Tables[0];

                        List<string> tempSupportList = new List<string>();

                        if (DT1.Rows.Count > 0)
                        {
                            for (int j = 0; j < DT1.Rows.Count; j++)
                            {
                                tempSupportList.Add(DT1.Rows[j]["vsupported_by"].ToString());
                            }
                        }
                        NpgsqlCommand cmd2;
                        cmd2 = new NpgsqlCommand();
                        cmd2.Connection = con;
                        cmd2.Parameters.Add(new NpgsqlParameter { ParameterName = "id", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = taskId });
                        cmd2.CommandText = "taskm.fn_show_action_details_task_id";
                        cmd2.CommandType = System.Data.CommandType.StoredProcedure;
                        NpgsqlDataAdapter ad2 = new NpgsqlDataAdapter(cmd2);
                        DataTable DT2 = new DataTable();
                        ad2.Fill(ds2);
                        DT2 = ds2.Tables[0];
                        List<string> tempActionList = new List<string>();
                        if (DT2.Rows.Count > 0)
                        {
                            for (int j = 0; j < DT2.Rows.Count; j++)
                            {
                                tempActionList.Add(DT2.Rows[j]["vaction"].ToString());
                            }
                        }
                        Tasklist.Add(new TaskD
                        {
                            TaskId = Convert.ToInt32((DT.Rows[i]["Vtask_id"].ToString())),
                            Task = (DT.Rows[i]["vtask"].ToString()),
                            ResponsibleName = (DT.Rows[i]["vresponsible_name"].ToString()),
                            CreatedAt = Convert.ToDateTime(DT.Rows[i]["vcreated_at"]),
                            Deadline = (DT.Rows[i]["vdeadline"].ToString()),
                            Category = (DT.Rows[i]["vcat_name"].ToString()),
                            Priority = (DT.Rows[i]["vpriority"].ToString()),
                            Status = (DT.Rows[i]["vstatus_type"].ToString()),
                            SubmittedOn = (DT.Rows[i]["vsubmitted_on"].ToString()),
                            Notes = (DT.Rows[i]["vnotes"].ToString()),
                            SupportedByName = tempSupportList,
                            Action = tempActionList
                        });
                        DT1.Clear();
                        DT2.Clear();
                    }
                }

                con.Close();
                res.Status = 200;
                res.Message = "Ok";
                res.Response = Tasklist;
                return res;
            }
            catch (Exception ex)//Show
            {
                string error = ex.Message + "";
                res.Status = 500;
                res.Message = error;
                res.Response = error;
                return res;
            }
        }
        public Reply ShowByCategoryName(string categoryName)//show task a/c to given category name
        {
            Reply res = new Reply();
            try
            {
                DataConnection datacon = new DataConnection();
                NpgsqlConnection con = datacon.pgconnect();
                NpgsqlCommand cmd;
                cmd = new NpgsqlCommand();
                cmd.Connection = con;
                cmd.Parameters.Add(new NpgsqlParameter { ParameterName = "cname", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar, Value = categoryName });
                cmd.CommandText = "taskm.fn_show_task_cat";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                NpgsqlDataAdapter ad = new NpgsqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                DataSet ds1 = new DataSet();
                DataSet ds2 = new DataSet();
                DataTable DT = new DataTable();
                ad.Fill(ds);
                DT = ds.Tables[0];
                List<TaskD> Tasklist = new List<TaskD>();
                if (DT.Rows.Count > 0)
                {
                    for (int i = 0; i < DT.Rows.Count; i++)
                    {
                        int taskId = Convert.ToInt32((DT.Rows[i]["Vtask_id"].ToString()));
                        NpgsqlCommand cmd1;
                        cmd1 = new NpgsqlCommand();
                        cmd1.Connection = con;
                        cmd1.Parameters.Add(new NpgsqlParameter { ParameterName = "id", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = taskId });
                        cmd1.CommandText = "taskm.fn_show_supported_by_task_id";
                        cmd1.CommandType = System.Data.CommandType.StoredProcedure;
                        NpgsqlDataAdapter ad1 = new NpgsqlDataAdapter(cmd1);
                        DataTable DT1 = new DataTable();
                        ad1.Fill(ds1);
                        DT1 = ds1.Tables[0];

                        List<string> tempSupportList = new List<string>();

                        if (DT1.Rows.Count > 0)
                        {
                            for (int j = 0; j < DT1.Rows.Count; j++)
                            {
                                tempSupportList.Add(DT1.Rows[j]["vsupported_by"].ToString());
                            }
                        }
                        NpgsqlCommand cmd2;
                        cmd2 = new NpgsqlCommand();
                        cmd2.Connection = con;
                        cmd2.Parameters.Add(new NpgsqlParameter { ParameterName = "id", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = taskId });
                        cmd2.CommandText = "taskm.fn_show_action_details_task_id";
                        cmd2.CommandType = System.Data.CommandType.StoredProcedure;
                        NpgsqlDataAdapter ad2 = new NpgsqlDataAdapter(cmd2);
                        DataTable DT2 = new DataTable();
                        ad2.Fill(ds2);
                        DT2 = ds2.Tables[0];
                        List<string> tempActionList = new List<string>();
                        if (DT2.Rows.Count > 0)
                        {
                            for (int j = 0; j < DT2.Rows.Count; j++)
                            {
                                tempActionList.Add(DT2.Rows[j]["vaction"].ToString());
                            }
                        }
                        Tasklist.Add(new TaskD
                        {
                            TaskId = Convert.ToInt32((DT.Rows[i]["Vtask_id"].ToString())),
                            Task = (DT.Rows[i]["vtask"].ToString()),
                            ResponsibleName = (DT.Rows[i]["vresponsible_name"].ToString()),
                            CreatedAt = Convert.ToDateTime(DT.Rows[i]["vcreated_at"]),
                            Deadline = (DT.Rows[i]["vdeadline"].ToString()),
                            Category = (DT.Rows[i]["vcat_name"].ToString()),
                            Priority = (DT.Rows[i]["vpriority"].ToString()),
                            Status = (DT.Rows[i]["vstatus_type"].ToString()),
                            SubmittedOn = (DT.Rows[i]["vsubmitted_on"].ToString()),
                            Notes = (DT.Rows[i]["vnotes"].ToString()),
                            SupportedByName = tempSupportList,
                            Action = tempActionList
                        });
                        DT1.Clear();
                        DT2.Clear();
                    }
                }

                con.Close();
                res.Status = 200;
                res.Message = "Ok";
                res.Response = Tasklist;
                return res;
            }
            catch (Exception ex)//Show
            {
                string error = ex.Message + "";
                res.Status = 500;
                res.Message = error;
                res.Response = error;
                return res;
            }
        }
        public Reply ShowBysupportedById(int empId)//show task a/c to given supporter emp
        {
            Reply res = new Reply();
            try
            {
                DataConnection datacon = new DataConnection();
                NpgsqlConnection con = datacon.pgconnect();
                NpgsqlCommand cmd;
                cmd = new NpgsqlCommand();
                cmd.Connection = con;
                cmd.Parameters.Add(new NpgsqlParameter { ParameterName = "id", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = empId });
                cmd.CommandText = "taskm.fn_show_emp_support_task";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                NpgsqlDataAdapter ad = new NpgsqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                DataSet ds1 = new DataSet();
                DataSet ds2 = new DataSet();
                DataTable DT = new DataTable();
                ad.Fill(ds);
                DT = ds.Tables[0];
                List<TaskD> Tasklist = new List<TaskD>();
                if (DT.Rows.Count > 0)
                {
                    for (int i = 0; i < DT.Rows.Count; i++)
                    {
                        int taskId = Convert.ToInt32((DT.Rows[i]["Vtask_id"].ToString()));
                        NpgsqlCommand cmd1;
                        cmd1 = new NpgsqlCommand();
                        cmd1.Connection = con;
                        cmd1.Parameters.Add(new NpgsqlParameter { ParameterName = "id", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = taskId });
                        cmd1.CommandText = "taskm.fn_show_supported_by_task_id";
                        cmd1.CommandType = System.Data.CommandType.StoredProcedure;
                        NpgsqlDataAdapter ad1 = new NpgsqlDataAdapter(cmd1);
                        DataTable DT1 = new DataTable();
                        ad1.Fill(ds1);
                        DT1 = ds1.Tables[0];

                        List<string> tempSupportList = new List<string>();

                        if (DT1.Rows.Count > 0)
                        {
                            for (int j = 0; j < DT1.Rows.Count; j++)
                            {
                                tempSupportList.Add(DT1.Rows[j]["vsupported_by"].ToString());
                            }
                        }
                        NpgsqlCommand cmd2;
                        cmd2 = new NpgsqlCommand
                        {
                            Connection = con
                        };
                        cmd2.Parameters.Add(new NpgsqlParameter { ParameterName = "id", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = taskId });
                        cmd2.CommandText = "taskm.fn_show_action_details_task_id";
                        cmd2.CommandType = System.Data.CommandType.StoredProcedure;
                        NpgsqlDataAdapter ad2 = new NpgsqlDataAdapter(cmd2);
                        DataTable DT2 = new DataTable();
                        ad2.Fill(ds2);
                        DT2 = ds2.Tables[0];
                        List<string> tempActionList = new List<string>();
                        if (DT2.Rows.Count > 0)
                        {
                            for (int j = 0; j < DT2.Rows.Count; j++)
                            {
                                tempActionList.Add(DT2.Rows[j]["vaction"].ToString());
                            }
                        }
                        Tasklist.Add(new TaskD
                        {
                            TaskId = Convert.ToInt32((DT.Rows[i]["Vtask_id"].ToString())),
                            Task = (DT.Rows[i]["vtask"].ToString()),
                            ResponsibleName = (DT.Rows[i]["vresponsible_name"].ToString()),
                            CreatedAt = Convert.ToDateTime(DT.Rows[i]["vcreated_at"]),
                            Deadline = (DT.Rows[i]["vdeadline"].ToString()),
                            Category = (DT.Rows[i]["vcat_name"].ToString()),
                            Priority = (DT.Rows[i]["vpriority"].ToString()),
                            Status = (DT.Rows[i]["vstatus_type"].ToString()),
                            SubmittedOn = (DT.Rows[i]["vsubmitted_on"].ToString()),
                            Notes = (DT.Rows[i]["vnotes"].ToString()),
                            SupportedByName = tempSupportList,
                            Action = tempActionList
                        });
                        DT1.Clear();
                        DT2.Clear();
                    }
                }

                con.Close();
                res.Status = 200;
                res.Message = "Ok";
                res.Response = Tasklist;
                return res;
            }
            catch (Exception ex)//Show
            {
                string error = ex.Message + "";
                res.Status = 500;
                res.Message = error;
                res.Response = error;
                return res;
            }
        }
    }
}
