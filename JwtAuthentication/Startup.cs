﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.IdentityModel.Tokens;

namespace JwtAuthentication
{

    public class Startup
    {
        public bool Isproduction { set; get; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
        .AddJwtBearer(options =>
        {
            options.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidateAudience = true,
                ValidateIssuerSigningKey = true,
                RequireExpirationTime = true,
                ValidateLifetime = true,
                ValidIssuer = Configuration["Jwt:Site"],
                ValidAudience = Configuration["Jwt:Site"],
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"])),
                ClockSkew = TimeSpan.Zero

            };
            options.Events = new JwtBearerEvents
            {
                OnChallenge = context =>
                {
                    context.Response.StatusCode = 401;
                    return Task.CompletedTask;
                }
            };
        });
            // add cors for allow origin
            services.AddCors(options =>
            {
                options.AddPolicy("AllowFromAll",
                    builder => builder
                    .WithMethods("GET", "POST")
                    .AllowAnyOrigin()
                    .AllowAnyHeader());
            }); ;

            services.AddMvc();
        }
        public IConfiguration Configuration { get; }



        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseCors("AllowFromAll");
            app.UseHttpsRedirection();
            app.UseStaticFiles();
           // app.UseStaticFiles(new StaticFileOptions
            //{
            //    FileProvider = new PhysicalFileProvider(
            //Path.Combine(Directory.GetCurrentDirectory(), "ErrorLog")),
            //    RequestPath = "/ErrorLog"
            //});

            app.UseAuthentication();

            app.UseMvc();
            // get connection string from appsetting json

            Isproduction = false;
            if (Isproduction == true)
            {
                ConnectionString = "Server=pdcwale.cbohidqlhnrl.us-east-2.rds.amazonaws.com;port=5432;Database=kderp;User Id=postgres;Password=pdcWale-R#2;Timeout=1024";
            }
            else
            {
                //ConnectionString = "Server=pdcwale.clo7emgkduun.us-east-2.rds.amazonaws.com;port=5432;Database=Trial;User Id=postgres;Password=pdcWale-R#2;Timeout=1024";//Trial work database
                ConnectionString = "Server=localhost;port=5432;Database=postgres;User Id=postgres;Password=Admin@1234;Timeout=1024";//Project Random quote + Task management
            }
        }
        public IConfigurationRoot ConConfiguration { get; set; }
        public static string ConnectionString { get; private set; }
    }
}

